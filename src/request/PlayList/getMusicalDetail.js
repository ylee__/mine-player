import instence from "../axios-instence";
function getMusicalDetail(id){
    return instence({
        url:`/playlist/detail?id=${id}`,
        method:'get'
    })
}
export default getMusicalDetail