import instence from "../axios-instence";
function getSearch(keyword){
    return instence({
        url:`/search?keywords=${keyword}`,
        methods: 'get'
    })
}
export default getSearch