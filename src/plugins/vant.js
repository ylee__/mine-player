import { Button , Swipe, SwipeItem,CellGroup,Cell,Popup,Field} from "vant";
let plugins=[Button, Swipe, SwipeItem,CellGroup,Cell,Popup,Field]
function getVant(app){
    plugins.forEach(element => {
        return app.use(element)
    });
}
export default getVant