import { createRouter,createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
  },
  {
    name:'MusicalListDtailPage',
    path:'/MusicalListDtailPage',
    component:() => import(/* webpackChunkName: "about" */ '../views/MusicalDetailPage.vue'),
    props($route){
      return{
        id:$route.query.id
      }
    }
  },
  {
    name:'search',
    path:'/search',
    component:() => import(/* webpackChunkName: "about" */ '../views/searchPage.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
