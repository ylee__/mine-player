import getMusicalDetail from '@/request/PlayList/getMusicalDetail'
export default {
    namespaced:true,
    state:{
        playList:[{
            al:{
            id:160034580,
            name:"天地无瑕",
            pic:109951168308704080,
            picUrl: "http://p3.music.126.net/u9moyflugpvCaZFic01zLw==/109951168308704081.jpg",
            pic_str: "109951168308704081",
            },
            id:2022319183
        }],
        name:"天地无瑕",
        isPlay:false,
        playIndex:0,
        showRight:false,//不弹出,
        picUrl:'',
        mode:0//默认是循环播放//1单曲循环/2是随机播放
    },
    actions:{
        updateplaylist(context,id){
            // 去发送请求
            console.log('效率不高')
            let a=getMusicalDetail(id)
            a.then((value)=>{
                let playlist=value.data.playlist.tracks
                context.commit('updatePlayList',playlist)
                console.log(playlist,"OOOO")
            })
        },
        clearAndUpadate({state},item){
            state.playList=[]
            state.playList.push(item)
            state.playIndex=0
            state.picUrl=state.playList[state.playIndex].album.artist.img1v1Url
            console.log(state)
        },
        updateIndex(context,index){
            context.state.playIndex=index
            context.state.picUrl=context.state.playList[context.state.playIndex].al.picUrl
        },
        updateMode({state},value){
            state.mode=value
        },
        setPlayIndex({state},value){
            state.playIndex=value
        },
        randomPlayIndex({state}){
            state.playIndex=Math.floor(Math.random()*state.playList.length)
        }
    },
    mutations:{
        setIsPlay(state,value){
            state.isPlay=value
        },
        // 更新歌单
        updatePlayList(state,value){
            state.playList=value
        },
        updateShowRight(state){
            state.showRight=!state.showRight
        },
        minIndex(state){
            state.playIndex--
            if(state.playIndex<0){state.playIndex=state.playList.length-1}
            console.log(state.playIndex)
            state.picUrl=state.playList[state.playIndex].al.picUrl
        },
        plusIndex(state){
            state.playIndex++
            if(state.playIndex===state.playList.length-1){
                state.playIndex=0
            }
            state.picUrl=state.playList[state.playIndex].al.picUrl
        }
    },
    getters:{

    }
}