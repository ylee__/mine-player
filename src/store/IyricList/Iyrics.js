
import getMusicalIyric from '@/request/PlayList/musicalLric'
const Lyric={
    namespaced:true,
    state:{
        audioCurrentTime:0,
        audioDurationTime:0,
        iyric:''
    },
    actions:{
        async getMusicalyrics(context,value){
            let i= await getMusicalIyric(value)
           context.state.iyric=i.data.lrc.lyric
        }
    },
    mutations:{
        updateAudioCurrentTime1(state,value){
            state.audioCurrentTime=value[0]
            state.audioDurationTime=value[1]
            // console.log(state.audioCurrentTime)
        },
        // isPaused(state,value){
        //     if(state.audioCurrentTime===state.audioDurationTime){
        //         state.isPaused=value
        //     }
        // }
    }
}
export default Lyric