import { createStore } from 'vuex'
import footerPlaylist from './bofangList/bofangList'
import Lyric from './IyricList/Iyrics'
export default createStore({
  modules: {
    footerPlaylist,
    Lyric
  }
})
