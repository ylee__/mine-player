const fs=require('fs')
// fs模块来对磁盘中的内容进行读写
// fs.readFile('./code/Promise封装/resouse/content.txt',(err,data)=>{
//     if(err) throw err
//     else{
//         console.log(data.toString())
//     }
// })

// 利用Promise来封装异步操作
let content
const p = new Promise((resovle,reject)=>{
    // 回调函数中进行异步操作
    fs.readFile('content.txt',(err,data)=>{
        if(err) reject(err)
        else{
            resovle(data)
        }
    })
})
p.then((value)=>{
    // 当p的状态是成功的时候，调用第一个回调
    console.log(value.toString())
    content=value.toString()
},(reason)=>{
    // 当p的状态是失败的时候，调用第二个回调
    console.log(reason)
})
console.log('keyi@:',content)
// 
// function minReadFile(path){
//     return new Promise((resovle,reject)=>{
//         fs.readFile(path,(err,data)=>{
//             if(err) reject(err)
//             else{
//                 resovle(data)
//             }
//         })
//     })
// }
// var fp=minReadFile('./code/Promise封装/resouse/content.txt')
// fp.then((value)=>{
//     console.log(value.toString())
// },(reason)=>{
//     console.log(reason)
// })

// function minWriteFile(path){
//     return new Promise((resovle,reject)=>{
//         fs.writeFile(path,'你好小金鱼',(err)=>{
//             if(err) reject(err)
//             else{
//                 resovle('写文件成功')
//             }
//         })
//     })
// }
// var fp=minWriteFile('hello.txt')
// fp.then((value)=>{
//     console.log(value)
// },(reason)=>{
//     console.log(reason)
// })