// 进行移动端的适配
function reSize(){
    const deviceWidth=document.documentElement.clientWidth||window.innerWidth
    if(deviceWidth>=750){
        deviceWidth=750
    }
    if(deviceWidth<=320){
        deviceWidth=320
    }
    // 设置htm元素的字体大小 1rem=10px
    document.documentElement.style.fontSize=(deviceWidth/7.5)+'px'
    // 设置body的元素字体大小
    document.querySelector('body').style.fontSize=0.3+'rem'
}
reSize()
// 当窗口发生变化时重新计算设备的宽度和根元素的字体大小，等比例的缩放
window.onresize=function(){
    reSize()
}